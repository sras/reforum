import { Component, Input } from '@angular/core'
import { Comment } from './story'

@Component({
  selector: 'comment-parent',
  template: `
    <div>
      <div class="parent-comment-content">
        <span *ngIf="comment.parent" class="comment-options">
          <span *ngIf="!showParentFlag">
            <a class="link" (click)="showParent(true)">Show parent</a>
          </span>
          <span *ngIf="showParentFlag">
            <a class="link" (click)="showParent(false)">Hide parent</a>
          </span>
        </span>
        <comment-parent
          *ngIf="showParentFlag"
          [comment]="comment.parent"></comment-parent>
        <div [innerHTML]= "comment.html||comment.story.title"></div>
      </div>
    <div>
  `,
  styles: [`
    .parent-comment-content {
      border-left: solid 3px #ff5722;
      padding: 10px;
      padding-top: 0px;
      padding-bottom: 20px;
      max-width: 800px;
      line-height: 1.1em;
      color: black;
    }
    .comment-options {
      font-size: 12px;
    }
    .link {
      color: black;
      text-decoration: none;
    }
  `
  ]
})
export class CommentParentComponent {
  @Input()
  comment: Comment

  showParentFlag: boolean = false

  showParent(flag) {
    this.showParentFlag = flag
  }
}
