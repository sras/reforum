import { Component, Input, OnChanges, OnDestroy } from '@angular/core'
import { Comment } from './story'
import { StoryService } from './story.service'
import { WatcherService } from './watcher.service'
import { Maybe } from './types'
import { CommentParentComponent } from './comment.parent.component'

@Component({
  selector: 'comment',
  template: `
    <div class="color-animation-6">
      <div class="comment-box">
        <div class="comment-options-container">
          <span class="user-name">{{comment.author.id}}</span>
          <span class="divider">|</span>
          <span class="comment-options">
            <a class="link" target="_BLANK" href="{{comment.link}}">
              {{comment.totalComments()}} Comments loaded
            </a>
          </span>
          <span *ngIf="comment.moreComments" class="comment-options">
            <span class="divider">|</span>
            <a class="link" (click)="loadMoreComments(comment)">Load more comments</a>
            <span class="divider">|</span>
            <a class="link" (click)="loadMoreComments(comment, true)">Load all comments</a>
          </span>
          <span class="comment-options" *ngIf="comment.comments && comment.comments.length > 0">
            <span *ngIf="!showGrandChildrenFlag && (showChildren || showChildrenOverrideFlag)">
              <span class="divider">|</span>
              <a class="link" (click)="showGrandChildren(true)">Expand child comments</a>
            </span>
            <span *ngIf="showGrandChildrenFlag && (showChildren || showChildrenOverrideFlag)">
              <span class="divider">|</span>
              <a class="link" (click)="showGrandChildren(false)">Top level comments only</a>
            </span>
            <span *ngIf="((showChildrenOverrideFlag===null) && showChildren===false) || (showChildrenOverrideFlag===false)">
              <span class="divider">|</span>
              <a class="link" (click)="showChildrenOverride(true)">Show children</a>
            </span>
            <span *ngIf="showChildrenOverrideFlag===true || (showChildren && showChildrenOverrideFlag === null)">
              <span class="divider">|</span>
              <a class="link" (click)="showChildrenOverride(false)">Hide children</a>
            </span>
          </span>
          <span class="comment-options">
            <span *ngIf="!showParentFlag">
              <span class="divider">|</span>
              <a class="link" (click)="showParent(true)">Show parent</a>
            </span>
            <span *ngIf="showParentFlag">
              <span class="divider">|</span>
              <a class="link" (click)="showParent(false)">Hide parent</a>
            </span>
          </span>
        </div>
        <comment-parent
          *ngIf="showParentFlag"
          [comment]="comment.parent"></comment-parent>
        <div class="comment-content" [innerHTML]="comment.html">
        </div>
        <div *ngIf="(showChildren && (showChildrenOverrideFlag===null)) || (showChildrenOverrideFlag===true)">
          <div *ngIf="comment.comments">
            <div *ngFor="let comment of comment.comments">
              <comment
                [showChildren]="showGrandChildrenFlag" [comment]="comment"></comment>
            </div>
            <span *ngIf="comment.moreComments" class="comment-options">
              <a class="link" (click)="loadMoreComments(comment)">Load more comments</a>
              <span class="divider">|</span>
              <a class="link" (click)="loadMoreComments(comment, true)">Load all comments</a>
            </span>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .comment-box {
      border-left: dashed 1px #ff8800;
      padding-left: 10px;
      margin: 10px
    }

    @media (max-width: 600px) {
      .comment-box {
        padding-left: 2px;
        margin: 4px
      }
    }
    .comment-content {
      padding-left: 12px;
      padding-bottom: 10px;
      max-width: 800px;
      color: black;
      line-height: 1.1em;
      font-size: 1.0em;
    }
    .user-name {
      color: #c73b31;
      padding-left: 0px;
      font-size: 14px;
    }
    .comment-options {
      color: black;
      font-size: 14px;
    }
    .link {
      color: black;
      text-decoration: none;
    }
    .divider {
      font-size: 14px;
      color: black;
    }
  `
  ]
})
export class CommentComponent implements OnChanges {
  @Input()
  comment: Comment

  @Input()
  showChildren: boolean

  showChildrenOverrideFlag: boolean = null
  showGrandChildrenFlag: boolean = true
  destroyed: boolean = false
  showParentFlag: boolean = false

  constructor(
    private watcherService: WatcherService,
    private storyService: StoryService) {}

  ngOnChanges(c) {
    if (c.hasOwnProperty('showChildren')) {
      if (c['showChildren']['currentValue'] === false) {
        this.showChildrenOverrideFlag = null
      }
    }
  }

  ngOnDestroy() {
    this.destroyed = true
  }

  showParent(flag) {
    this.showParentFlag = flag
  }

  showGrandChildren(s) {
    this.showGrandChildrenFlag = s
  }

  showChildrenOverride(s) {
    this.showChildrenOverrideFlag = s
  }

  watch(comment: Comment) {
    this.watcherService.watchItem(comment)
  }

  loadMoreComments(item: Comment, full: boolean = false) {
    this.storyService.loadMoreComments(item).
      ifAvailableDo((fo) => {
        fo.subscribe(
          (comment) => {
            this.comment = comment
            this.showChildrenOverrideFlag = true
            if (full) {
              if (this.comment.moreComments && !this.destroyed) {
                this.loadMoreComments(this.comment, true)
              }
            }
          }
        )
        return Maybe.Nothing()
    })
  }
}
