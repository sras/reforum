import { CommentComponent } from './comment.component'
import { Observable }  from 'rxjs/Observable';
import { Component, OnInit, OnDestroy, Input } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { StoryService } from './story.service'
import { FullStory } from './story'
import { Comment } from './story'
import { Maybe } from './types'
import { WatcherService } from './watcher.service'
import { EntityDecodePipe } from './entity.decode.pipe'
import 'rxjs/add/observable/throw'

@Component({
  selector: 'merged-story-component',
  template: `
    <div *ngIf="fullStories">
      <h2 class="title">
        <a rel="noopener noreferrer" target="_BLANK" href="{{fullStories[0].story.link}}">
          {{ fullStory.stories[0].title }}
        </a>
      </h2>
      <div class="comment-options">
        <span *ngIf="fullStory.moreComments">
          <span class="divider">|</span>
          <a class="link" (click)="loadMoreComments(fullStory)">Load more comments</a>
        </span>
        <span *ngIf="fullStory.moreComments">
          <span class="divider">|</span>
          <a class="link" (click)="loadMoreComments(fullStory, true)">Load all comments</a>
        </span>

        <span *ngIf="!showGrandChildrenFlag">
          <span class="divider">|</span>
          <a class="link" (click)="showGrandChildren(true)">Expand child comments</a>
        </span>
        <span *ngIf="showGrandChildrenFlag">
          <span class="divider">|</span>
          <a class="link" (click)="showGrandChildren(false)">Top level comments only</a>
        </span>
      </div>
      <div *ngFor="let comment of allComments">
        <comment
          [comment]="comment" [showChildren]="showGrandChildrenFlag"></comment>
      </div>
    </div>
  `,
  styles: [`
    .title {
      margin-top: 0px;
      margin-bottom: 0px;
      color: white;
    }
    .content {
      border-radius: 10px;
      padding: 10px;
      background-color: #202020;
      border: 1px solid #030303;
    }
    .comment-options {
      font-size: 12px;
    }
    .username {
      font-size: 12px;
      color: #2a9fd6;
    }
    .link {
      color: #e9322d;
      text-decoration: none;
    }
  `]
})
export class StoryComponent implements OnInit, OnDestroy {
  private fullStory: FullStory = null
  private showGrandChildrenFlag: boolean = true
  private loadMoreCommentsInProgress: (FullStory|Comment)[] = []
  private destroyed = false

  @Input()
  private story = null

  constructor(
    private storyService: StoryService,
    private watcherService: WatcherService,
    private route: ActivatedRoute
  ) {}

  showGrandChildren(s) {
    this.showGrandChildrenFlag = s
  }

  loadMoreComments(item: FullStory|Comment, full: boolean = false) {
    if (this.loadMoreCommentsInProgress.indexOf(item) !== -1) {
      return
    } 
    this.loadMoreCommentsInProgress.push(item)
    this.storyService.loadMoreComments(item).
      ifAvailableDo((fo) => {
        let subscription = fo.
        catch((e: any) => {
          let index = this.loadMoreCommentsInProgress.indexOf(item)
          this.loadMoreCommentsInProgress.splice(index, 1)
        }).
        subscribe(
          (full_story) => {
            let index = this.loadMoreCommentsInProgress.indexOf(item)
            this.loadMoreCommentsInProgress.splice(index, 1)
            this.fullStory = full_story
            if (full) {
              if (this.fullStory.moreComments && !this.destroyed) {
                this.loadMoreComments(this.fullStory, true)
              }
            }
          }
        )
    })
  }

  ngOnDestroy() {
    this.destroyed = true
  }

  watch(comment: Comment) {
    this.watcherService.watchItem(comment)
  }

  ngOnInit() {
    if (this.story === null) {
      this.route.params.subscribe(params => this.loadId(params['id']))
    } else {
      this.loadId(this.story.id)
    }
  }

  private loadId(id: string) {
    this.storyService.getFullStoryFromId(id).
      subscribe((fs) => {
        this.fullStory = fs
      })
  }
}
