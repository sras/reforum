import { Directive, ElementRef, HostListener, Input, Renderer, Inject } from '@angular/core'
import { CacheService } from './cache.service'

function getScrollTop(w) {
  return w.scrollY || w.document.documentElement.scrollTop
}

@Directive({selector: '[myScrollSave]'})
export class ScrollSaveDirective {
  private debounceCount: number = 0
  private debounceLimit: number = 10

  constructor(
    @Inject('Window') private win,
    private cacheService: CacheService,
    private el: ElementRef,
    private renderer: Renderer) {}

  @HostListener('window:scroll') onWindowScroll() {
    this.debounceCount += 1
    if (this.debounceCount > this.debounceLimit) {
      let sp = getScrollTop(this.win)
      this.cacheService.setKey('stories-scroll-pos', sp)
      this.debounceCount = 0
    }
  }
}
