import { Component, OnInit, OnChanges, AfterViewInit, Input, Inject } from '@angular/core'
import { WatcherService } from './watcher.service'
import { StoryService } from './story.service'
import { LogService } from './log.service'
import { Story } from './story'
import { ScrollSaveDirective } from './scroll.save.directive'
import { CacheService } from './cache.service'
import { UtilsService } from './utils.service'
import { StoryItemComponent } from './story.item.component'

function restoreScroll(w, pos) {
  w.document.body.scrollTop = pos
}

@Component({
  selector: 'stories-component',
  template: `
    <div
      *ngIf="stories===null">Loading...</div>
    <div
      myScrollSave
      *ngIf="sources" class="container">
      <source-component [stories]="stories" class="source" *ngFor="let stories of sources">
      </source-component>
    </div>
  `,
  styles: [`
    .container {
      margin-left: auto;
      margin-right: auto;
    }
    .source {
      float: left;
      width: 100%;
    }
    .story {
      border-radius: 5px;
      border: 1px solid #282828;
      margin-top: 10px;
      padding: 5px;
      background-color: #222222;
    }
    .comment-count {
      color: red;
    }
    
    .link {
      margin-left: 10px;
      margin-right: 10px;
      color: #888888;
    }
  `]
})

export class StoriesComponent implements OnInit, OnChanges, AfterViewInit {
  sources: Story[][][] = null
  private storiesRaw: Story[] = null

  @Input()
  triggerReload: boolean

  constructor(
    @Inject('Window') private win,
    private cacheService: CacheService,
    private watcherService: WatcherService,
    private storyService: StoryService,
    private logService: LogService,
    private utilsService: UtilsService
    ) {}

  private updateStories(stories: Story[]) {
    if (this.storiesRaw === null) {
      this.storiesRaw = stories;
    } else {
      this.utilsService.mergeStories(this.storiesRaw, stories)
    }
    this.sources = this.
      utilsService.
      groupStoriesBySource(
          this.utilsService.groupStories(this.storiesRaw))
  }

  ngOnInit() {
    this.storyService.getStories().
      subscribe(stories => {this.updateStories(stories)})
    this.watcherService.
      getStoriesObserver().
      subscribe(
        (stories) => {
        }
      )
  }

  ngAfterViewInit() {
    let last_scroll_pos = this.cacheService.getKey('stories-scroll-pos')
    if (last_scroll_pos) {
      restoreScroll(this.win, last_scroll_pos)
    }
  }

  ngOnChanges(c) {
    if (c.hasOwnProperty('triggerReload')) {
      if (typeof(c['triggerReload']['previousValue']) === 'boolean') {
        this.storyService.getStories().
          subscribe(stories => { this.updateStories(stories) })
      }
    }
  }
}
