import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { MainComponent } from './main.component';
import { MainRoutes } from './main.routes';

export const appRoutes: Routes = [
  {
    path: "",
    component: MainComponent,
    children: [
      ...MainRoutes
    ]
  }
];


export const appRoutingProviders: any[] = [

];

export const routes: ModuleWithProviders = RouterModule.forRoot(appRoutes);
