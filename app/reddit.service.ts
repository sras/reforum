import { Injectable } from '@angular/core'
import { Response } from '@angular/http'
import { Observable }  from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/concat';
import { Story } from './story'
import { User } from './story'
import { Comment } from './story'
import { FullStory } from './story'
import { ConfigService } from './config.service'
import { HttpService } from './http.service'
import { Maybe } from './types'

@Injectable()
export class RedditService {

  constructor(
    private httpService: HttpService,
    private config: ConfigService
    ) {}

  public getStories(): Observable<Story[]> {
    let subnames = this.config.getConfig('reddit')['subnames']
    return subnames.map((x) => this.getStoriesForsub(x)).reduce((a, i) => {
      if (a===null) {
        return i
      } else {
        return a.concat(i)
      }
    }, Observable.from([]))
  }

  public fetchFullStory(id: string): Maybe<Observable<FullStory>> {
    let [forum, sub, story_id] = id.split('-')
    if (forum && forum === 'reddit') {
      let url = `https://www.reddit.com/r/${sub}/comments/${story_id}.json?raw_json=1`
      return Maybe.Just(this.fetchFullStoryFromUrl(url, sub))
    } else {
      return Maybe.Nothing()
    }
  }

  public loadMoreComments(item: FullStory | Comment): Maybe<Observable<FullStory>> | Maybe<Observable<Comment>> {
    let root
    if (item instanceof FullStory) {
      root = item
    } else if (item instanceof Comment) {
      root = this.findRoot(item)
    }
    let contained_ids = this.getContainedIds(item)
    let m = this._loadMoreComments(item, root)
    if (m.isNothing()) {
      let m = this.loadMoreCommentsForChildren(item, root)
      if (m.isNothing()) {
        item.moreComments = false
      }
      this.bottomUpMarkMoreComments(root, root)
      return m
    } else {
      return m.ifAvailableDo(obs => {
        return Maybe.Just(obs.map((item: FullStory|Comment) => {
          let new_contained_ids = this.getContainedIds(item)
          if (new_contained_ids.length > contained_ids.length) {
            item.moreComments = this.hasMoreComments(item, root) || this.doChildrenHaveMoreComments(item, root)
          } else {
            item.moreComments = this.doChildrenHaveMoreComments(item, root)
          }
          this.bottomUpMarkMoreComments(root, root)
          return item
        }))
      })
    }
  }

  private _loadMoreComments(item: FullStory | Comment, root: FullStory): Maybe<Observable<FullStory>> | Maybe<Observable<Comment>> {
    let child_ids: string[]
    if (item instanceof FullStory) {
      child_ids = this.findMoreChildIds(item, item)
    } else if (item instanceof Comment) {
      child_ids = this.findMoreChildIds(item, root)
    }
    if (child_ids.length > 0)  {
      let unloaded_ids = this.getUnloadedIds(root, child_ids)
      if (unloaded_ids.length > 0) {
        let observable = this.fetchComments(unloaded_ids, root).
          map(([comments, more_items]) => {
            if (comments.length > 0) {
              for (let c of comments) {
                this.insertComment(root, c)
              }
              this.storeLoadedCommentIds(root)
              this.updateMoreItems(root, more_items)
            } else {
              for (let id of unloaded_ids) {
                this.resetMoreItemsFor(this.getId(item), root)
              }
            }
            return item
          })
        return Maybe.Just(observable)
      }
    } else {
      return this.loadDeepChildren(item, root)
    }
    return Maybe.Nothing()
  }

  private doChildrenHaveMoreComments(item: FullStory|Comment, root: FullStory): boolean {
    for (let comment of item.comments) {
      if (this.hasMoreComments(comment, root) || this.doChildrenHaveMoreComments(comment, root)) {
        return true
      } else {
        continue
      }
    }
    return false
  }

  private loadMoreCommentsForChildren(item: FullStory | Comment, root: FullStory): Maybe<Observable<FullStory>> | Maybe<Observable<Comment>> {
    for (let comment of item.comments) {
      let m = this.loadMoreComments(comment)
      if (m.isNothing()) {
        continue
      } else {
        return m.ifAvailableDo(ob => Maybe.Just(ob.map(_ => item)) )
      }
    }
    return Maybe.Nothing()
  }

  private hasMoreComments(item: FullStory|Comment, root: FullStory): boolean {
    let child_ids = this.findMoreChildIds(item, root)
    let unloaded_ids = this.getUnloadedIds(root, child_ids)
    return unloaded_ids.length > 0 || this.hasDeepChildren(item, root)
  }

  private resetMoreItemsFor(id: string, root: FullStory) {
    let mi = this.getMoreItems(root)
    delete mi[id]
    this.storeMoreItems(root, mi)
  }

  private loadDeepChildren(item: FullStory|Comment, root: FullStory): Maybe<Observable<Comment>> {
    if (item instanceof Comment) {
      if (this.hasDeepChildren(item, root)) {
        let obs: Observable<Comment> = this.
          fetchFullStoryFromUrl(`${item.link}.json?raw_json=1`).
          map(fs => {
            this.resetDeepChildrenFlagFor(item, root)
            item.comments = item.comments.concat(fs.comments[0].comments)
            this.mergeMoreItems(root, fs)
            this.storeLoadedCommentIds(root)
            this.markMoreComments(item, root)
            return item
          })
        return Maybe.Just(obs)
      }
    }
    return Maybe.Nothing()
  }

  private mergeMoreItems(fs1: FullStory, fs2: FullStory) {
    let fs1_mi = this.getMoreItems(fs1)
    let fs2_mi = this.getMoreItems(fs2)
    for (let i in fs2_mi) {
      if (fs2_mi.hasOwnProperty(i)) {
        fs1_mi[i] = fs2_mi[i]
      }
    }
    this.storeMoreItems(fs1, fs1_mi)
  }

  private hasDeepChildren(item: FullStory|Comment, root: FullStory): boolean {
    let id = this.getId(item)
    let more_items = this.getMoreItems(root)
    if (more_items.hasOwnProperty(id)) {
      return more_items[id]['id'] === '_'
    } else {
      return false
    }
  }

  private resetDeepChildrenFlagFor(item: FullStory|Comment, root: FullStory) {
    let id = this.getId(item)
    let more_items = this.getMoreItems(root)
    if (more_items.hasOwnProperty(id)) {
      if (more_items[id]['id'] === '_') {
        delete more_items[id]
      }
    }
  }

  private getUnloadedIds(root: FullStory, ids: string[]): string[] {
    let loaded = this.getLoadedCommentIds(root)
    return ids.filter(x => !loaded.hasOwnProperty('t1_'+ x))
  }

  private findNode(parent: FullStory|Comment, id: string): Maybe<FullStory|Comment> {
    if (this.getId(parent) === id) {
      return Maybe.Just(parent)
    } else {
      for (let comment of parent.comments) {
        let m_node = this.findNode(comment, id)
        if (m_node.isNothing()) {
          continue
        } else {
          return m_node
        }
      }
    }
    return Maybe.Nothing()
  }

  private insertComment(root: FullStory, comment: Comment) {
    let parent_id = comment.rawData['parent_id']
    this.findNode(root, parent_id).ifAvailableDo((p_node) => {
      p_node.comments.push(comment)
      comment.parent = p_node
    })
  }

  private fetchComments(ids: string[], root: FullStory): Observable<[Comment[], {}]> {
    let link_id = this.getId(root)
    let url = `https://www.reddit.com/api/morechildren.json?children=${ids.join(',')}&link_id=${link_id}&raw_json=1`;
    return this.httpService.get(url).
        map((response: Response) => {
          let data = response.json()['jquery']
          let children = data[data.length - 1][3][0]
          return this.makeCommentsFromData(children, null, root)
        }).catch(this.handleError)
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error'
    return Observable.throw(errMsg)
  }

  private fetchFullStoryFromUrl(url: string, section: string=""): Observable<FullStory> {
    return this.httpService.get(url)
      .map(response => {
          let json = response.json()
          return this.makeFullStoryFromResponse(json, section)
          }).
    catch(this.handleError)
  }

  private makeFullStoryFromResponse(json: any, section: string=""): FullStory {
    let story = this.makeStoryFromData(json[0]['data']['children'][0]['data'], section)
    let post = json[0].data.children[0].data
    let text = post.selftext
    let html = post.selftext_html
    let full_story = new FullStory(
        story,
        text,
        html,
        null,
        null,
        json)
    let children_data = json[1]['data']['children']
    let more_items:{}
    [full_story.comments, more_items] = this.makeCommentsFromData(children_data, full_story, full_story)
    this.storeMoreItems(full_story, more_items)
    this.storeLoadedCommentIds(full_story)
    //this.markMoreComments(full_story, full_story)
    this.bottomUpMarkMoreComments(full_story, full_story)
    return full_story
  }

  private storeLoadedCommentIds(full_story: FullStory) {
    let dict = {}
    let c = 0
    for (let i of this.getContainedIds(full_story)) {
      dict[i] = c
      c += 1
    }
    full_story.rawData['loaded_ids'] = dict
  }

  private getLoadedCommentIds(full_story: FullStory) {
    return full_story.rawData['loaded_ids']
  }

  private getContainedIds(item: FullStory|Comment) {
    let ids = [this.getId(item)]
    for (let c of item.comments) {
      ids = ids.concat(this.getContainedIds(c))
    }
    return ids
  }

  private markMoreComments(item: FullStory|Comment, root_node: FullStory) {
    item.moreComments = this.hasMoreComments(item, root_node)
    for (let c of item.comments) {
      this.markMoreComments(c, root_node)
    }
  }

  private bottomUpMarkMoreComments(item: FullStory|Comment, root_node: FullStory) {
    this.resetMoreComments(item)
    let leafs = this.getLeafNodes(item)
    for (let leaf of leafs) {
      this.markMoreCommentsUptoRoot(leaf, root_node, false)
    }
  }

  private resetMoreComments(item: FullStory|Comment) {
    item.moreComments = false
    for (let c of item.comments) {
      this.resetMoreComments(c)
    }
  }

  private markMoreCommentsUptoRoot(item: FullStory|Comment, root:FullStory, mark:boolean) {
    if (!mark) {
      mark = this.hasMoreComments(item, root)
    } 
    if (!item.moreComments) {
      item.moreComments = mark
    }
    if (item instanceof Comment) {
      this.markMoreCommentsUptoRoot(item.parent, root, mark)
    }
  }

  private getLeafNodes(item: FullStory|Comment) {
    let leaf_nodes = []
    if (!item.comments || item.comments.length === 0) {
      leaf_nodes.push(item)
    } else {
      for (let c of item.comments) {
        leaf_nodes = leaf_nodes.concat(this.getLeafNodes(c))
      }
    }
    return leaf_nodes
  }

  private makeCommentFromData(wdata: {}, parent_node: FullStory|Comment, root_node: FullStory): [Comment, {}] {
    let data = wdata['data']
    let this_comment = new Comment(
        data['id'], 
        'reddit',
        new User(data['author'], data['author']),
        `${root_node.story.forumLink}${data['id']}`,
        data['body'],
        data['body_html'],
        null,
        null,
        parent_node,
        data
      )
    let more_items: {}
    if (data['replies'] instanceof Object) {
      [this_comment.comments, more_items] = this.makeCommentsFromData(data['replies']['data']['children'], this_comment, root_node)
    } else {
      this_comment.comments = []
      more_items = {}
    }
    return [this_comment, more_items]
  }

  private makeCommentsFromData(data:{}[], parent_node: FullStory|Comment, root_node: FullStory): [Comment [], {}] {
    let comments_and_more: [Comment, {}][] = data.
      filter((x) => x['kind'] !== 'more').
      map((x) => this.makeCommentFromData(x, parent_node, root_node))
    let more_items = this.collectMoreItems(data)
    let comments = []
    for (let c of comments_and_more) {
      comments.push(c[0])
      let mi = c[1]
      for (let k in mi) {
        if (mi.hasOwnProperty(k)) {
          more_items[k]  = mi[k]
        }
      }
    }
    return [comments, more_items]
  }

  private storeMoreItems(item: FullStory, more_items:{}) {
    item.rawData['more_items'] = more_items
  }

  private getMoreItems(item: FullStory): {} {
    if (item.rawData.hasOwnProperty('more_items')) {
      return  item.rawData['more_items']
    } else {
      return {}
    }
  }

  private updateMoreItems(item: FullStory, more_items:{}) {
    let e_mi = item.rawData['more_items']
    for (let m in more_items) {
      if (more_items.hasOwnProperty(m)) {
        e_mi[m] = more_items[m]
      }
    }
  }

  private getId(item: FullStory|Comment): string {
    if (item instanceof FullStory) {
      return item.rawData[0]['data']['children'][0]['data']['name']
    } else if (item instanceof Comment) {
      return `t1_${item.id}`
    }
  }

  private findMoreChildIds(item: FullStory|Comment, root: FullStory): string[] {
    let id = this.getId(item)
    let more_items = this.getMoreItems(root)
    if (more_items.hasOwnProperty(id)) {
      return more_items[id]['children']
    } else {
      return []
    }
  }

  private findRoot(item: FullStory|Comment) {
    if (item instanceof FullStory) {
      return item
    } else {
      return this.findRoot(item.parent)
    }
  }

  private collectMoreItems(children: {}[]) : {} {
    return this.convertToAssociation(this._collectMoreItems(children), 'parent_id')
  }

  private _collectMoreItems(children: {}[]) : {}[] {
    return children.filter((x) => { return x['kind'] === 'more' }).map(x => x['data'])
  }

  private convertToAssociation(data: {}[], key: string): {} {
    let ret = {}
    for (let i of data) {
      ret[i[key]] = i
    }
    return ret
  }

  private getStoriesForsub(subname): Observable<Story[]> {
    let url = (subname === null) ?  
      `https://www.reddit.com/.json`:
      `https://www.reddit.com/r/${subname}/.json?raw_json=1`
    return this.httpService.get(url)
      .map((x) => {
        return this.extractSubredditStories(x, subname)
      })
      .catch(this.handleError)
  }

  private extractSubredditStories(res: Response, subname: string) : Story[] {
    let body = res.json()
    let stories =  body.data.children.map(item => this.makeStoryFromData(item.data, subname) )
    return stories
  }

  private makeStoryFromData(r: {}, subname: string='') {
    return new Story('reddit', `/r/${subname}`, `reddit-${r['subreddit']}-${r['id']}`, r['title'], r['created_utc'], r['url'], `https://www.reddit.com${r['permalink']}`, new User(r['author'], r['author']), parseInt(r['num_comments']), r['selftext_html'],  r)
  }
}
