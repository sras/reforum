import { Injectable } from '@angular/core'
import { Observable }  from 'rxjs/Observable'
import { Message, InfoMessage } from './log'

@Injectable()
export class LogService {

  private messages: {} = {}

  private subscribers: any[] = []

  subscribe(callback: (any) => any): Observable<string> {
    return Observable.create((subscriber) => {
      this.subscribers.push(subscriber)
    }).subscribe(callback)
  }

  remove(message_id: string) {
    if (this.messages.hasOwnProperty(message_id)) {
      delete this.messages[message_id]
      this.notify()
    }
  }

  amend(message_id: string, callback: (Message)=>Message) {
    if (this.messages.hasOwnProperty(message_id)) {
      let m = this.messages[message_id]
      this.messages[message_id] = callback(m)
      this.notify()
    }
  }

  notify() {
    let all_messages = Object.keys(this.messages).map(key => this.messages[key])

    for (let s of this.subscribers) {
      s.next(all_messages)
    }
  }

  clear() {
    this.messages = {}
    this.notify()
  }

  log(message: string): string {
    let m_id = String(Date.now())
    this.messages[m_id] = new InfoMessage(message)
    this.notify()
    return m_id
  }
}
