export abstract class Message {
  constructor(public message) {
  }

  abstract type(): string
}

export class InfoMessage extends Message {
  type(): string {
    return 'info'
  }
}


export class ErrorMessage extends Message {
  type(): string {
    return 'error'
  }
}
