import { Injectable } from '@angular/core'
import { Observable }  from 'rxjs/Observable';
import { LogService } from './log.service'
import { ErrorMessage } from './log'
import { Http, Response, Headers, RequestOptions } from '@angular/http'

@Injectable()
export class HttpService {

  constructor(
    private http: Http,
    private logService: LogService
    ) {}

  get(url: string): Observable<any> {
    let log_id = this.logService.log(`Fetching ${url}`)
    return this.http.get(url).
      map(response => {
        let json = response.json()
        if (json) {
          this.logService.remove(log_id)
          return response
        } else {
          this.logService.amend(log_id, ({message: x}) => new ErrorMessage(x + ': Server returned empty response'))
          return null
        }
      }).catch((error: any) => {
      let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error'
        this.logService.amend(log_id, ({message: x}) => new ErrorMessage(x + ':' + errMsg))
        return Observable.from([])
      })
  }
}
