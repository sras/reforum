import { Component, OnInit, OnChanges, AfterViewInit, Input, Inject } from '@angular/core'
import { WatcherService } from './watcher.service'
import { StoryService } from './story.service'
import { LogService } from './log.service'
import { Story } from './story'
import { ScrollSaveDirective } from './scroll.save.directive'
import { CacheService } from './cache.service'
import { UtilsService } from './utils.service'
import { StoryItemComponent } from './story.item.component'

@Component({
  selector: 'source-component',
  template: `
    <div class="wrapper">
      <div style="font-size: 2em">{{forumspec}}</div>
      <div
        *ngIf="stories" class="container">
        <ol>
          <li *ngFor="let story of stories">
            <div class="story">
              <story-item *ngIf="story.length == 1"  [story]="story[0]">
              </story-item>
              <merged-story-item *ngIf="story.length > 1"  [stories]="story">
              </merged-story-item>
            </div>
          </li>
        </ol>
      </div>
    </div>
  `,
  styles: [`
    .wrapper {
      padding: 5px;
      color: #2c2d30;
    }
    .container {
      width: 100%;
      overflow: auto;
    }
    .story {
      margin-top: 0px;
      padding: 1px;
    }
    .comment-count {
      color: red;
    }
    
    .link {
      margin-left: 10px;
      margin-right: 10px;
      color: #888888;
    }
  `]
})

export class SourceComponent implements OnInit {
  @Input()
  stories: Story[][] = null
  forumspec: string = null

  ngOnInit() {
    this.forumspec = this.stories.reduce((acc: [string], stories: Story[]) => {
      let forum_spec = stories.map(x => `${x.forum}: ${x.section}`)
      if (!acc) {
        return forum_spec
      } else {
        return acc.filter(x => forum_spec.indexOf(x) !== -1)
      }
    }, null)[0]
  }
}
