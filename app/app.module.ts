import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import
       { AppComponent }  from './app.component';

import { MainComponent } from './main.component';
import { StoryService } from './story.service'
import { RedditService } from './reddit.service'
import { HackernewsService } from './hackernews.service'
import { CacheService } from './cache.service'
import { RedditConfigService } from './reddit.config.service'
import { HackernewsConfigService } from './hackernews.config.service'
import { WatcherService } from './watcher.service'
import { HttpService } from './http.service'
import { LogService } from './log.service'
import { ConfigWatcherService } from './config.watcher.service';
import { LocalstorageService } from './localstorage.service';
import { NotificationService } from './notification.service';
import { UtilsService } from './utils.service';
import { StoriesComponent } from './stories.component';
import { SourceComponent } from './source.component';
import { MergedStoryItemComponent } from './merged.story.item.component';
import { MergedStoryCommentsComponent } from './merged.story.comments.component';
import { StoryComponent } from './story.component';
import { ConfigComponent } from './config.component';
import { SetConfigComponent } from './set.config.component';
import { LogComponent } from './log.component';
import { RedditConfigComponent } from './reddit.config.component';
import { HackernewsConfigComponent } from './hackernews.config.component';
import { ConfigService } from './config.service';
import { WatcherComponent } from './watcher.component';
import { StoryItemComponent } from './story.item.component';
import { TimeformatPipe } from './timeformat.pipe';
import { CommentComponent } from './comment.component';
import { CommentParentComponent } from './comment.parent.component';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { routes,
         appRoutingProviders } from './app.routes';
import {
  LocationStrategy,
  HashLocationStrategy
} from '@angular/common';

@NgModule({
  imports: [ BrowserModule, FormsModule, HttpModule, routes],
  declarations: [ TimeformatPipe, SourceComponent, MainComponent, AppComponent, MergedStoryItemComponent, StoryItemComponent, WatcherComponent, RedditConfigComponent, HackernewsConfigComponent, LogComponent, StoryComponent, CommentComponent, CommentParentComponent, StoriesComponent, ConfigComponent, SetConfigComponent, MergedStoryCommentsComponent],
  bootstrap:    [ AppComponent ],
  providers: [appRoutingProviders, UtilsService, NotificationService, ConfigWatcherService, LocalstorageService, HttpService, LogService, WatcherService, CacheService, StoryService, ConfigService, RedditConfigService, HackernewsConfigService, RedditService, HackernewsService, 
  {provide: LocationStrategy, useClass: HashLocationStrategy},
  {provide: ('Window'), useValue: window}
  ]
})


export class AppModule { }
