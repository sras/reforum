export class Maybe<T> {
  content: T 
  valid: boolean

  constructor(content: T) {
    this.content = content
    this.valid = this.content !== null
  }

  static Just(content: any) {
    return new Maybe(content)
  }

  static Nothing() {
    return new Maybe(null)
  }

  isNothing(): boolean {
    return !this.valid
  }

  ifAvailableDo<B>(fn: (T) => (Maybe<B>|void)): Maybe<B> {
    if (this.valid) {
      let x = fn(this.content)
      if (x instanceof Maybe) {
        return x
      } else {
        return Maybe.Nothing()
      }
    } else {
      return new Maybe(null)
    }
  }
}
