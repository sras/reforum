import { Injectable } from '@angular/core'
import { Observable }  from 'rxjs/Observable';
import { RedditService } from './reddit.service'
import { HackernewsService } from './hackernews.service'
import { Story } from './story'
import { Comment } from './story'
import { FullStory } from './story'
import { Maybe } from './types'
import { UtilsService } from './utils.service'
import 'rxjs/add/operator/merge';

function shuffleArray(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

@Injectable()
export class StoryService {
  private stories: Maybe<Story[]>
  private softResetFlag: boolean = false

  constructor(
      private redditService: RedditService,
      private hackernewsService: HackernewsService,
      private utilsService: UtilsService
      ) {
    this.stories = Maybe.Nothing()
  }

  private _getStories(): Observable<Story[]> {
    return this.redditService.getStories().merge(this.hackernewsService.getStories())
  }

  loadMoreComments(item: FullStory | Comment): Maybe<Observable<FullStory>> | Maybe<Observable<Comment>> {
    let forum = this.getForum(item)
    if (forum === 'reddit') {
      return this.redditService.loadMoreComments(item)
    } else if (forum === 'hackernews') {
      return this.hackernewsService.loadMoreComments(item)
    }
  }

  private getForum(item: FullStory | Comment): string {
    if (item instanceof Comment) {
      return item.forum
    } else if (item instanceof FullStory) {
      return item.story.forum
    }
  }

  getFullStoryFromId(id: string): Observable<FullStory> {
    return Observable.create(subscriber => {
      let callback = (ob_story) => {
        ob_story.subscribe(story => subscriber.next(story))
        return Maybe.Just(true)
      }
      if (!this.redditService.
        fetchFullStory(id).
        ifAvailableDo(callback).isNothing()) return

      if (!this.hackernewsService.
        fetchFullStory(id).
        ifAvailableDo(callback).isNothing()) return
    })
  }

  getStoryItem(id: string): Maybe<Story> {
    if (this.stories.isNothing()) {
      return Maybe.Nothing()
    } else {
      return this.stories.ifAvailableDo((stories) => {
        for (let story of stories) {
          if (story.id === id) {
            return Maybe.Just(story)
          }
        }
      })
    }
  }

  reset() {
    this.stories = Maybe.Nothing()
  }

  softReset() {
    this.softResetFlag = true
  }

  getStories(): Observable<Story[]> {
    return Observable.create(subscriber => {
      if (this.stories.isNothing()) {
        this.stories = Maybe.Just([])
        this._getStories().
          subscribe(
            stories => {
              this.stories = this.stories.ifAvailableDo(sl => {
                  let all_stories = sl.concat(stories)
                  subscriber.next(all_stories)
                  return Maybe.Just(all_stories)
              })
            },
            () => { console.log('an error occured!') },
            () => {
              subscriber.complete()
            }
          )
      } else {
        this.stories.ifAvailableDo(x => {
          if (x.length === 0) {
            this.stories = Maybe.Nothing()
            this.getStories().subscribe(x => subscriber.next(x))
          } else {
            if (this.softResetFlag) {
              this.softResetFlag = false
              let temp_stories  = this.stories
              this.reset()
              this._getStories().subscribe(stories => {
                if (temp_stories.isNothing()) {
                  subscriber.next(stories)
                } else {
                  temp_stories.ifAvailableDo((t_stories) => {
                    this.utilsService.mergeStories(t_stories, stories)
                    subscriber.next(t_stories)
                    this.stories = Maybe.Just(t_stories)
                  })
                }
              })
            } else {
              subscriber.next(x)
            }
          }
          return Maybe.Nothing()
        })
      }
    })
  }
}
