import { Component, Input, Output } from '@angular/core'
import { Story } from './story'
import { StoryComponent } from './story.component'
import { TimeformatPipe } from './timeformat.pipe'

@Component({
  selector: 'story-item',
  template: `
      <div class="story">
        <span style="cursor:pointer" (click)="show()">{{story.title}}</span>
        <small>{{story.timestamp|timeFormat}}</small>
        <small> in {{story.section}}</small>
        <div class="links">
          <a class="link" rel="noopener noreferrer" target="_BLANK" href="{{story.link}}">link</a>
          <a class="link" target="_BLANK" href="{{story.forumLink}}">{{story.replyCount}} comments</a>
          <a class="link" *ngIf="story.content && !showContentFlag && !showInlineFlag" (click)="showContent(true)">Show selftext</a>
          <a class="link" *ngIf="story.content && showContentFlag && !showInlineFlag" (click)="showContent(false)">Hide selftext</a>
          <a class="link" *ngIf="!showInlineFlag" (click)="showInline(true)">Show inline</a>
          <a class="link" *ngIf="showInlineFlag" (click)="showInline(false)">Hide inline</a>
        </div>
        <div
          *ngIf="showContentFlag"
          class="content" [innerHTML]="story.content"></div>
        <div class="popup" *ngIf="showInlineFlag">
          <div class="popup-content">
            <story-component [story]="story"></story-component>
          </div>
        </div>
        <div
          style="margin-top:10px;margin-bottom:10px;"
          *ngIf="showContentFlag">
          <a class="link" *ngIf="!showInlineFlag" (click)="showInline(true)">Show whole thread inline</a>
          <a class="link" *ngIf="story.content && showContentFlag" (click)="showContent(false)">Hide selftext</a>
        </div>
      </div>
  `,
  styles: [`
    .popup {
      width: 90%;
    }
    .popup-content {
      position: relative;
      width: 100%;
    }
    
    .rotate {

    /* Safari */
    -webkit-transform: rotate(-90deg);

    /* Firefox */
    -moz-transform: rotate(-90deg);

    /* IE */
    -ms-transform: rotate(-90deg);

    /* Opera */
    -o-transform: rotate(-90deg);

    /* Internet Explorer */
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

    }
    .links {
      font-size: 12px;
    }
    .content {
      font-size:  1.0em;
      max-width: 800px;
      padding-top: 5px;
      line-height: 1.1em;
      padding-left: 5px;
      border-left: solid 5px #f76134;
      margin-top: 10px;
    }
    .story {
      color: black;
      font-size: 1.0em;
      line-height: 1.0rem;
      padding: 0px;
      margin: 0px;
      background-color: #fff;
    }
    .comment-count {
      color: red;
    }
    .link {
      font-size: 16px;
      margin-left: 10px;
      margin-right: 10px;
      color: #e9322d;
    }
  `]
})

export class StoryItemComponent {

  @Input()
  story: Story

  @Output()


  showContentFlag: boolean = false
  showInlineFlag: boolean = false

  show() {
    if (this.story.content) {
      if (this.showContentFlag) {
        this.showInlineFlag = true;
        this.showContentFlag = false;
      } else {
        if (this.showInlineFlag) {
          this.showContentFlag = false;
          this.showInlineFlag = false;
        } else {
          this.showContentFlag = true;
        }
      }
    } else {
        this.showInlineFlag = !this.showInlineFlag;
    }
  }

  showContent(flag) {
    this.showContentFlag = flag
  }

  showInline(flag) {
    this.showInlineFlag = flag
    if (flag) {
      this.showContentFlag = false
    }
  }
}
