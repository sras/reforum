import { Injectable } from '@angular/core'
import {Observable} from 'rxjs/Observable'
import {Subject} from 'rxjs/Subject'
import 'rxjs/add/observable/timer'
import { Story, FullStory, Comment, WatchItem, Change } from './story'
import { StoryService } from './story.service'

@Injectable()
export class NotificationService {
  notifications: {} = {}

  addNotification(n: any) {
    let m_id = String(Date.now())
    this.notifications[m_id] = n
    return m_id
  }

  remove(n_id: string) {
    delete this.notifications[n_id]
  }

  getNotifications(): any[] {
    let ret = []
    for (let n in this.notifications) {
      if (this.notifications.hasOwnProperty(n)) {
        ret.push({id: n, notification: this.notifications[n]})
      }
    }
    return ret
  }
}
