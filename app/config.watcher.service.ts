import { Injectable } from '@angular/core'
import { LocalstorageService } from './localstorage.service'
import { ConfigService } from './config.service'

@Injectable()
export class ConfigWatcherService {
  constructor(
    private configService: ConfigService,
    private localstorageService: LocalstorageService
    ) {
  }

  start() {
    //let reddit_config = this.localstorageService.get('reddit', '[]')
    //if (reddit_config && false) {
    //  this.configService.setConfig({reddit: JSON.parse(reddit_config).join(',')})
    //}

    //let hackernews_config = this.localstorageService.get('hackernews', '[]')
    //if (hackernews_config && false) {
    //  this.configService.setConfig({hackernews: JSON.parse(hackernews_config).join(',')})
    //}
    //let refresh_interval = parseInt(this.localstorageService.get('refreshinterval', '10'))
    //if (refresh_interval > 0) {
    //  this.configService.setConfig({refreshinterval: refresh_interval })
    //}

    this.configService.
      getObserver().
      subscribe((config) => {
        this.localstorageService.set(config['key'], JSON.stringify(config['config']))
      })
  }
}
