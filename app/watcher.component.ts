import { CommentComponent } from './comment.component'
import { Component, OnInit} from '@angular/core'
import { StoryService } from './story.service'
import { FullStory } from './story'
import { Comment, WatchItem } from './story'
import { WatcherService } from './watcher.service'
import { NotificationService } from './notification.service'
import 'rxjs/add/observable/throw'

@Component({
  template: `
    <div *ngIf="watchItems.length > 0">
      <h2>Watching</h2>
      <div class="pure-g" *ngFor="let wi of watchItems">
        <div class="pure-u-1-3">Watching for new comments in <i>{{wi.watchItem}}</i></div>
        <div class="pure-u-1-3">
          <button (click)="unwatch(wi)">Unwatch</button>
        </div>
      </div>
    </div>
    <div *ngIf="watchItems.length == 0">
      No items are being watched!
    </div>
    <div>
      <h2>Notifications</h2>
      <div class="pure-g" *ngFor="let ni of notifications">
        <div class="pure-u-1-1">
          {{ni.notification.description}}
        </div>
      </div>
    </div>
  `
})
export class WatcherComponent implements OnInit {
  watchItems: {} [] = []
  notifications: {} [] = []

  constructor(
    private watcherService: WatcherService,
    private notificationService: NotificationService
    ) {}

  ngOnInit() {
    this.watchItems = this.watcherService.getWatchedItems()
    this.notifications = this.notificationService.
      getNotifications()
  }

  unwatch(wi: WatchItem) {
    this.watcherService.unwatch(wi)
    this.watchItems = this.watcherService.getWatchedItems()
  }

}
