import { Injectable } from '@angular/core'
import { Response } from '@angular/http'
import { Observable }  from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/concat';
import 'rxjs/add/observable/zip';
import { Story } from './story'
import { User } from './story'
import { Comment } from './story'
import { FullStory } from './story'
import { ConfigService } from './config.service'
import { HttpService } from './http.service'
import { Maybe } from './types'

@Injectable()
export class HackernewsService {

  constructor(
    private httpService: HttpService,
    private config: ConfigService
    ) {}

  getStories(): Observable<Story[]> {
    return this._getStories()
  }

  private getOneStoryFromId(id: string): Observable<FullStory> {
    return Observable.create((subscriber) => {
      this.getOneStory(id).
        subscribe((story) => {
          let fs =new FullStory(
              story,
              story.rawData['text'], 
              story.rawData['text'], 
              null,
              story.rawData.hasOwnProperty('kids'),
              story.rawData)
          if (fs.story.replyCount === 0) {
            subscriber.next(fs)
            subscriber.complete()
          } else { 
            let c = this.loadMoreComments(fs)
            if (c.isNothing()) {
              subscriber.next(fs)
            } else {
              c.ifAvailableDo(ob_fs => {
                ob_fs.subscribe(fs => {
                  subscriber.next(fs)
                  subscriber.complete()
                })
              })
            }
          }
        })
      })
  }

  fetchFullStory(id: string): Maybe<Observable<Story>> {
    let [forum, story_id] = id.split('-')
    if (forum && forum === 'hackernews') {
      return Maybe.Just(this.getOneStoryFromId(story_id))
    } else {
      return Maybe.Nothing()
    }
  }

  loadMoreComments(item: FullStory | Comment): Maybe<Observable<FullStory>> | Maybe<Observable<Comment>> {
    let kid_ids = this.collectLeafIds(item)
    return Maybe.Just(Observable.create((subscriber) => {
      this.getComments(kid_ids).subscribe((comments) => {
        for (let c of comments) {
          this.insertComment(item, c)
        }
        this.markMoreItems(item)
        subscriber.next(item)
        subscriber.complete()
      })
    }))
  }

  refreshComment(item: Comment): Observable<Comment> {
    return this.getComment(item.id)
  }

  private markMoreItems(item: FullStory | Comment) {
    item.moreComments = this.collectLeafIds(item).length > 0
    if (item.comments) {
      for (let c of item.comments) {
        this.markMoreItems(c)
      }
    }
  }

  private insertComment(item: FullStory | Comment, comment: Comment) {
    let parent_id = comment.rawData['parent']
    this.findNode(item, parent_id).ifAvailableDo(parent => {
        if (parent.comments === null) {
          parent.comments = [comment]
        } else {
          parent.comments.push(comment)
        }
        comment.parent = parent
      }
    )
  }

  private findNode(item: FullStory | Comment, id: string): Maybe<FullStory | Comment> {
    if (item.rawData['id'] === id) {
      return Maybe.Just(item)
    } else {
      if (item.comments instanceof Object) {
        for (let c of item.comments) {
          let n = this.findNode(c, id)
          if (n.isNothing()) {
            continue
          } else {
            return n
          }
        }
      }
    }
    return Maybe.Nothing()
  }

  private collectLeafIds(item: FullStory | Comment): string[] {
    if (item.comments === null) {
      if (item.rawData.hasOwnProperty('kids')) {
        return item.rawData['kids']
      } else {
        return []
      }
    } else {
      let t = item.comments.
        map(x => this.collectLeafIds(x))
      let g = t.reduce((a, i) => a.concat(i), [])
      return g
    }
  }

  private getComments(ids: string[]): Observable<Comment[]> {
    let obs: Observable<Comment>[] = ids.map(x => this.getComment(x))
    return Observable.zip.apply(null, obs).map(comments => comments.filter(x => x))
  }

  private getComment(id: string): Observable<Comment> {
    let url = `https://hacker-news.firebaseio.com/v0/item/${id}.json`
    return this.httpService.get(url).map(x => {
      return this.makeComment(x)
    })
  }

  private _getStories(): Observable<Story[]> {
    let pages = this.config.getConfig('hackernews')['pagenames']
    return pages.map((x) => this.getStoriesForPage(x)).reduce((a, i) => {
      if (a===null) {
        return i
      } else {
        return a.concat(i)
      }
    }, Observable.from([]))
  }

  private makeComment(r: any): Comment {
    if (r) {
      let d = r.json()
      return new Comment(
          d['id'],
          'hackernews',
          new User(d['by'], d['by']),
          `https://news.ycombinator.com/item?id=${d['id']}`,
          d['text'],
          d['text'],
          null,
          d.hasOwnProperty('kids'),
          null,
          d)
    } else {
      return null
    }
  }

  private getStoriesForPage(page: string = 'Hackernews'): Observable<Story[]> {
    let url = `https://hacker-news.firebaseio.com/v0/${page}.json`
    return Observable.create((subscriber) => {
      this.httpService.get(url).subscribe(
        r => {
          return this.extractStories(r, page).
          subscribe(stories => {
            subscriber.next(stories)
            subscriber.complete()
          })
        })
    })
  }

  private extractStories(r: Response, pagename: string): Observable<Story[]> {
    let story_ids: string[] = r.json().slice(0, 30)
    let observables = story_ids.map(x => this.getOneStory(x, pagename))
    return Observable.zip.apply(null, observables).map(y => y.filter(x => x))
  }

  private getOneStory(id: string, pagename: string = 'Hackernews'): Observable<Story> {
    let url = `https://hacker-news.firebaseio.com/v0/item/${id}.json`
    return this.httpService.get(url).map((x) => {
      return this.extractStory(x, pagename)
    })
  }

  private extractStory(wdata: Response, pagename:string): Story {
    if (wdata) {
      let data = wdata.json()
      let url = data['url']
      if (url === undefined || url.length === 0) {
        url = `https://news.ycombinator.com/item?id=${data['id']}`
      }
      return new Story(
          'hackernews',
          `${pagename}` ,
          `hackernews-${data['id']}`, 
          data['title'], 
          data['time'],
          url,
          `https://news.ycombinator.com/item?id=${data['id']}`,
          new User(data['by'], data['by']),
          data['descendants']?parseInt(data['descendants']):0,
          data['text'],
          data
        )
    } else {
      return null
    }
  }
}
