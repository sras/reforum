import { Component } from '@angular/core'
import { HackernewsConfigService } from './hackernews.config.service'
import { StoryService } from './story.service'

@Component({
  selector: 'hackernews-config',
  template: `
    <h3 style="margin:0px">Hackernews config</h3>
    <small>Page name can be one of 'newstories', 'topstories', 'beststories', 'askstories', 'showstories', 'jobstories'
    as documented <a href="https://github.com/HackerNews/API">here</a></small>
    <div>
      <div *ngFor="let pagename of hackernewsConfigService.pageNames">
        <div>
          {{pagename}} <a class="link" (click)="removePage(pagename)">Remove</a>
        </div>
      </div>
      <input
        class="input"
        placeholder="Add new page"
        type="text" [(ngModel)]="newPage">
      <a class="button" (click)="addPage(newPage)">Add new</a>
      <br>
    </div>
  `,
  styles: [`
    .link {
      text-decoration: underline;
    }
    .input {
      margin-top: 5px;
      background-color: #f7f7f7;
      border-radius: 5px;
      border: none;
      padding: 5px;
      color: black;
    }
    .button {
      cursor: pointer;
      margin: 10px;
      margin-left: 0px;
      display: block;
      max-width: 100px;
      padding: 5px;
      font-size: 70%;
      margin-right: 5px;
      border-radius: 5px;
      color: #ffffff;
      background-color: #424242;
      border-color: #424242;
    }
  `]
})

export class HackernewsConfigComponent {
  public newPage: string = null

  constructor(
      private hackernewsConfigService: HackernewsConfigService,
      private storyService: StoryService
      ) {}

  removePage(pagename) {
    this.hackernewsConfigService.removePage(pagename)
    this.storyService.reset()
  }

  addPage(pagename) {
    this.hackernewsConfigService.addPage(pagename)
    this.storyService.reset()
    this.newPage = ""
  }

}
