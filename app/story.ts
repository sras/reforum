export class Comment {
  constructor(
    public id: string,
    public forum: 'reddit'|'hackernews', 
    public author: User,
    public link: String,
    public text: String,
    public html: String,
    public comments: Comment[],
    public moreComments: boolean,
    public parent: Comment | FullStory,
    public rawData: {}
  ) {}

  totalComments(): number {
    return totalComments(this)
  }

  toString(): string {
    return this.text.substring(0, 30)
  }
}

export class Story {
  constructor(
      public forum: 'reddit'|'hackernews', 
      public section: string,
      public id: string,
      public title: string,
      public timestamp: String,
      public link: string,
      public forumLink: string,
      public poster: User,
      public replyCount: number,
      public content: string ,
      public rawData: {}
      ) {}

  toString(): string {
    return this.title
  }
}

export class User {
  constructor(public id: string, public name: string) {}
}

export class FullStory {
  constructor(
    public story: Story,
    public text: string,
    public html: string,
    public comments: Comment[],
    public moreComments: boolean,
    public rawData: {}
    ) {}

  totalComments(): number {
    return totalComments(this)
  }

  toString(): string {
    return this.story.toString()
  }
}

export class WatchItem {
  constructor(public watchItem: (FullStory|Comment), public watchType: string) {}
}

export class Change {
  constructor(
      public item: WatchItem,
      public change: string,
      public content: any,
      public description: any
      ) {}
}


function totalComments(item: FullStory|Comment): number {
    if (item.comments) {
      return item.comments.length + 
        item.comments.
          map(x => totalComments(x)).
          reduce((a, i) => a + i, 0)
    } else {
      return 0
    }
  }
