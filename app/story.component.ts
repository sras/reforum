import { CommentComponent } from './comment.component'
import { Observable }  from 'rxjs/Observable';
import { Component, OnInit, OnDestroy, Input } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { StoryService } from './story.service'
import { FullStory } from './story'
import { Comment } from './story'
import { Maybe } from './types'
import { WatcherService } from './watcher.service'
import { EntityDecodePipe } from './entity.decode.pipe'
import 'rxjs/add/observable/throw'

@Component({
  selector: 'story-component',
  template: `
    <div 
      class="content"
      [innerHTML]="story.content"
      *ngIf="!fullStory && story">
    </div>
    <div *ngIf="fullStory">
      <div class="content" *ngIf="fullStory.html" [innerHTML]="fullStory.html"></div>
      <div class="comment-options">
        <span class="username">{{ fullStory.story.poster.name }}</span>
        <span class="divider">|</span>
        <a class="link" rel="noopener noreferrer" target="_BLANK" href="{{fullStory.story.forumLink}}">
          <span>{{ fullStory.totalComments() }} of {{ fullStory.story.replyCount }}</span> Comments loaded
        </a>
        <span *ngIf="fullStory.moreComments">
          <span class="divider">|</span>
          <a class="link" (click)="loadMoreComments(fullStory)">Load more comments</a>
        </span>
        <span *ngIf="fullStory.moreComments">
          <span class="divider">|</span>
          <a class="link" (click)="loadMoreComments(fullStory, true)">Load all comments</a>
        </span>

        <span *ngIf="!showGrandChildrenFlag">
          <span class="divider">|</span>
          <a class="link" (click)="showGrandChildren(true)">Expand child comments</a>
        </span>
        <span *ngIf="showGrandChildrenFlag">
          <span class="divider">|</span>
          <a class="link" (click)="showGrandChildren(false)">Top level comments only</a>
        </span>
      </div>
      <div *ngFor="let comment of fullStory.comments">
        <comment
          [comment]="comment" [showChildren]="showGrandChildrenFlag"></comment>
      </div>
      <div *ngIf="fullStory.story.replyCount > 0" class="comment-options">
        <span>{{ fullStory.story.replyCount }}</span> Comments
        <span class="divider">|</span>
        <span>{{ fullStory.totalComments() }}</span> Comments loaded
        <span *ngIf="fullStory.moreComments">
          <span class="divider">|</span>
          <a class="link" (click)="loadMoreComments(fullStory)">Load more comments</a>
        </span>
        <span *ngIf="fullStory.moreComments">
          <span class="divider">|</span>
          <a class="link" (click)="loadMoreComments(fullStory, true)">Load all comments</a>
        </span>

        <span *ngIf="!showGrandChildrenFlag">
          <span class="divider">|</span>
          <a class="link" (click)="showGrandChildren(true)">Expand child comments</a>
        </span>
        <span *ngIf="showGrandChildrenFlag">
          <span class="divider">|</span>
          <a class="link" (click)="showGrandChildren(false)">Top level comments only</a>
        </span>
      </div>
    </div>
  `,
  styles: [`
    .title {
      margin-top: 0px;
      margin-bottom: 0px;
      color: white;
    }
    .content {
      margin-top: 10px;
      padding-top: 5px;
      color: #202020;
      line-height: 1.1em;
      padding-left: 5px;
      border-left: solid 5px #f76134;
    }
    .comment-options {
      font-size: 12px;
    }
    .username {
      font-size: 12px;
      color: #2a9fd6;
    }
    .link {
      color: #e9322d;
      text-decoration: none;
    }
  `]
})
export class StoryComponent implements OnInit, OnDestroy {
  private fullStory: FullStory = null
  private showGrandChildrenFlag: boolean = true
  private loadMoreCommentsInProgress: (FullStory|Comment)[] = []
  private destroyed = false

  @Input()
  private story = null

  constructor(
    private storyService: StoryService,
    private watcherService: WatcherService,
    private route: ActivatedRoute
  ) {}

  showGrandChildren(s) {
    this.showGrandChildrenFlag = s
  }

  loadMoreComments(item: FullStory|Comment, full: boolean = false) {
    if (this.loadMoreCommentsInProgress.indexOf(item) !== -1) {
      return
    } 
    this.loadMoreCommentsInProgress.push(item)
    this.storyService.loadMoreComments(item).
      ifAvailableDo((fo) => {
        let subscription = fo.
        catch((e: any) => {
          let index = this.loadMoreCommentsInProgress.indexOf(item)
          this.loadMoreCommentsInProgress.splice(index, 1)
        }).
        subscribe(
          (full_story) => {
            let index = this.loadMoreCommentsInProgress.indexOf(item)
            this.loadMoreCommentsInProgress.splice(index, 1)
            this.fullStory = full_story
            if (full) {
              if (this.fullStory.moreComments && !this.destroyed) {
                this.loadMoreComments(this.fullStory, true)
              }
            }
          }
        )
    })
  }

  ngOnDestroy() {
    this.destroyed = true
  }

  watch(comment: Comment) {
    this.watcherService.watchItem(comment)
  }

  ngOnInit() {
    if (this.story === null) {
      this.route.params.subscribe(params => this.loadId(params['id']))
    } else {
      this.loadId(this.story.id)
    }
  }

  private loadId(id: string) {
    this.storyService.getFullStoryFromId(id).
      subscribe((fs) => {
        this.fullStory = fs
      })
  }
}
