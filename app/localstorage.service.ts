import { Injectable } from '@angular/core'

@Injectable()
export class LocalstorageService {
  set(key: string, value: string) {
    localStorage.setItem(key, value)
  }

  get(key: string, def: string="") {
    let i = localStorage.getItem(key)
    if (i === null) {
      return def
    } else {
      return i
    }
  }
}
