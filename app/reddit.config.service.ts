import { Injectable } from '@angular/core'
import { Observable }  from 'rxjs/Observable';

@Injectable()
export class RedditConfigService {
  subNames: string[] = [];
  subscribers: any[] = []

  setConfig(cstr: string) {
    this.setSubs(cstr.split(',').filter(x => x.length>0))
    this.notify()
  }

  getConfig(): {} {
    return {subnames: this.subNames}
  }

  setSubs(subnames: string[]) {
    for (let sname of subnames) {
      if (sname.substr(0,1) === '-') {
        this._removeSub(sname.substr(1));
      } else {
        this._addNewSub(sname)
      }
    }
    this.notify()
  }

  private _removeSub(subname: string) {
    let index = this.subNames.indexOf(subname)
    if (index !== -1) {
      this.subNames.splice(index, 1)
    }
  }

  private _addNewSub(subname: string) {
    if (this.subNames.indexOf(subname) === -1) {
      this.subNames.push(subname)
    }
  }

  removeSub(subname: string) {
    this._removeSub(subname)
    this.notify()
  }

  addNewSub(subname: string) {
    this._addNewSub(subname)
    this.notify()
  }

  getObserver() {
    return Observable.create((subscriber) => {
      this.subscribers.push(subscriber)
    })
  }

  notify() {
    for (let s of this.subscribers) {
      s.next({'key': 'reddit', 'config': this.subNames})
    }
  }
}

