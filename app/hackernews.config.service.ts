import { Injectable } from '@angular/core'
import { Observable }  from 'rxjs/Observable';

@Injectable()
export class HackernewsConfigService {
  pageNames: string[] = [];
  subscribers: any[] = []

  setConfig(cstr: string) {
    this.pageNames = cstr.split(',').filter(x => x.length > 0)
    this.notify()
  }

  setPages(pages: string[]) {
    for (let sname of pages) {
      if (sname.substr(0,1) === '-') {
        this._removePage(sname.substr(1));
      } else {
        this._addPage(sname)
      }
    }
    this.notify()
  }

  getConfig(): {} {
    return {pagenames: this.pageNames}
  }

  private _removePage(pagename) {
    let index = this.pageNames.indexOf(pagename)
    if (index !== -1) {
      this.pageNames.splice(index, 1)
    }
  }

  private _addPage(subname) {
    if (this.pageNames.indexOf(subname) == -1) {
      this.pageNames.push(subname)
    }
  }

  removePage(pagename) {
    this._removePage(pagename)
    this.notify()
  }

  addPage(pagename) {
    this._addPage(pagename)
    this.notify()
  }

  getObserver() {
    return Observable.create((subscriber) => {
      this.subscribers.push(subscriber)
    })
  }

  notify() {
    for (let s of this.subscribers) {
      s.next({'key': 'hackernews', 'config': this.pageNames})
    }
  }
}

