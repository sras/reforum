import { Component } from '@angular/core';

@Component({
  selector: 'reforum-app',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent { }
