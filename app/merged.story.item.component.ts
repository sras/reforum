import { Component, Input } from '@angular/core'
import { Story } from './story'
import { StoryComponent } from './story.component'
import { TimeformatPipe } from './timeformat.pipe'

@Component({
  selector: 'merged-story-item',
  template: `
      <div class="story" >
        <story-item *ngFor="let story of stories" [story]=story>
        </story-item>
      </div>
      <div>
        <a class="link"
          *ngIf="!showInlineFlag"
          (click)="showInline(true)">Show merged inline</a>
        <a class="link"
          *ngIf="showInlineFlag"
          (click)="showInline(false)">Hide inline</a>
      </div>
      <div class="popup" *ngIf="showInlineFlag">
        <div class="popup-content">
          <merged-story-comments-component
            [stories]="stories"
            *ngIf="showInlineFlag"></merged-story-comments-component>
        </div>
      </div>
  `,
  styles: [`
    .popup {
      width: 90%;
    }
    .popup-content {
      position: relative;
      width: 100%;
    }
    .rotate {

    /* Safari */
    -webkit-transform: rotate(-90deg);

    /* Firefox */
    -moz-transform: rotate(-90deg);

    /* IE */
    -ms-transform: rotate(-90deg);

    /* Opera */
    -o-transform: rotate(-90deg);

    /* Internet Explorer */
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

    }
    .divider {
      font-size: 14px;
    }
    .link {
      font-size: 14px;
      color: #e9322d;
    }
  `]
})

export class MergedStoryItemComponent {
  @Input()
  stories: Story[] = []
  showInlineFlag: boolean = false

  getIds(): string[] {
    return this.stories.map(x => x.id)
  }

  showInline(flag: boolean) {
    this.showInlineFlag = flag
  }
}
