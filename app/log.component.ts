import { Component, OnInit } from '@angular/core';
import { LogService } from './log.service'

@Component({
  selector: 'log',
  template: `
  <div *ngIf="messages.length > 0" class="pure-g fixed">
    <div *ngFor="let m of messages" [class.error]="m.type() === 'error'" class="pure-u-1 message">
      {{m.message}}
    </div>
    <div class="pure-u-1 message">
      <a class="clear" (click)="clear()">Clear</a>
    </div>
  <div>
  `,
  styles: [`
    .fixed {
      position: fixed;
      bottom: 0px;
      right: 0px;
      background-color: rgba(0, 0, 0, 0.65);
      color: white;
      padding: 10px;
      z-index: 1000;
    }
    .clear {
      float: right;
      color:red;
      text-decoration: underline;
      cursor: pointer;
    }
    .message {
      font-size: 14px;
    }

    .error {
      color: red;
    }
  `]
})
export class LogComponent implements OnInit {
  private messages: any[] = []

  constructor(private logService: LogService) {}

  ngOnInit() {
    this.logService.subscribe((messages) => {
      this.messages = messages
    })
  }

  clear() {
    this.logService.clear()
  }

}
