import { CommentComponent } from './comment.component'
import { Observable }  from 'rxjs/Observable';
import 'rxjs/add/observable/zip';
import { Component, OnInit, OnDestroy, Input } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { StoryService } from './story.service'
import { FullStory } from './story'
import { Comment } from './story'
import 'rxjs/add/observable/throw'

@Component({
  selector: 'merged-story-comments-component',
  template: `
    <div class="content" *ngIf="fullStories">
      <div class="comment-options">
        <span *ngIf="hasMoreComments()">
          <a class="link" (click)="loadMoreComments()">Load more comments</a>
          <span class="divider">|</span>
        </span>
        <span *ngIf="hasMoreComments()">
          <a class="link" (click)="loadMoreComments(true)">Load all comments</a>
          <span class="divider">|</span>
        </span>

        <span *ngIf="!showGrandChildrenFlag">
          <a class="link" (click)="showGrandChildren(true)">Expand child comments</a>
        </span>
        <span *ngIf="showGrandChildrenFlag">
          <a class="link" (click)="showGrandChildren(false)">Top level comments only</a>
        </span>
      </div>
      <div class="comment-options" *ngFor="let fullStory of fullStories">
        <span class="username">{{ fullStory.story.poster.name }}</span><span> in {{ fullStory.story.section }}</span>
        &nbsp;
        <a class="link" rel="noopener noreferrer" target="_BLANK" href="{{fullStory.story.forumLink}}">
          <span>{{ fullStory.totalComments() }} of {{ fullStory.story.replyCount }}</span> Comments loaded
        </a>
      </div>
      <div *ngFor="let comment of commentsMerged">
        <comment
          [comment]="comment" [showChildren]="showGrandChildrenFlag"></comment>
      </div>
    </div>
  `,
  styles: [`
    .title {
      margin-top: 0px;
      margin-bottom: 0px;
      color: white;
    }
    .content {
      padding-top: 5px;
      color: #202020;
      line-height: 1.1em;
      padding-left: 5px;
    }
    .comment-options {
      font-size: 12px;
    }
    .username {
      font-size: 12px;
      color: #2a9fd6;
    }
    .link {
      color: black;
      font-size: 1.2em;
      text-decoration: none;
    }
  `]
})
export class MergedStoryCommentsComponent implements OnInit, OnDestroy {
  private fullStories: FullStory[] = null
  private commentsMerged: Comment[] = null
  private showGrandChildrenFlag: boolean = true
  private loadMoreCommentsInProgress: boolean = false
  private destroyed = false

  @Input()
  private stories = null

  constructor(
    private storyService: StoryService,
    private route: ActivatedRoute
  ) {}

  showGrandChildren(s) {
    this.showGrandChildrenFlag = s
  }

  loadMoreComments(full: boolean = false) {
    if (this.loadMoreCommentsInProgress) {
      return
    } 
    let i = 0;
    for (let item of this.fullStories) {
      if (item.moreComments) {
        this.loadMoreCommentsInProgress = true
        this.storyService.loadMoreComments(item).
          ifAvailableDo((fo) => {
            let subscription = fo.
            catch((e: any) => {
              this.loadMoreCommentsInProgress = false
            }).
            subscribe(
              (full_story) => {
                this.fullStories[i] = full_story
                this.makeMergedComments()
                this.loadMoreCommentsInProgress = false
                if (full) {
                  if (this.hasMoreComments() && !this.destroyed) {
                    this.loadMoreComments(true)
                  }
                }
              }
            )
        })
        return
      }
      i += 1
    }
  }

  ngOnDestroy() {
    this.destroyed = true
  }

  ngOnInit() {
    if (this.stories === null) {
      this.route.params.subscribe(params => {
          this.loadIds(params['ids'].split(','))
        }
      )
    } else {
      this.loadIds(this.stories.map(x => x.id))
    }
  }

  makeMergedComments() {
    this.commentsMerged = []
    for (let fs of this.fullStories) {
      this.commentsMerged = this.commentsMerged.concat(fs.comments)
    }
  }

  hasMoreComments() {
    if (this.fullStories) {
      for (let fs of this.fullStories) {
        if (fs.moreComments) {
          return true
        }
      }
    }
    return false
  }

  private loadIds(ids: string[]) {
    let obs: Observable<FullStory>[] = ids.map(x => this.loadId(x))
    Observable.zip.apply(null, obs).subscribe(full_stories => {
      this.fullStories = full_stories
      this.makeMergedComments()
    })
  }

  private loadId(id: string): Observable<FullStory> {
    return this.storyService.getFullStoryFromId(id)
  }
}
