import { Injectable } from '@angular/core'
import { Observable }  from 'rxjs/Observable';
import { Subject }  from 'rxjs/Subject';
import { RedditConfigService } from './reddit.config.service'
import { HackernewsConfigService } from './hackernews.config.service'

@Injectable()
export class ConfigService {
  private refreshInterval: number = null
  private changesSubject: Subject<{}> = null

  constructor(
    public redditConfigService: RedditConfigService,
    public hackernewsConfigService: HackernewsConfigService
  ) {
    this.changesSubject = new Subject() as Subject<{}>

    this.redditConfigService.getObserver().
      merge(this.hackernewsConfigService.getObserver()).
      subscribe(changes => this.changesSubject.next(changes))
  }

  setConfig(config: {}) {
    if (config.hasOwnProperty('reddit')) {
      this.redditConfigService.setConfig(config['reddit'])
    }
    if (config.hasOwnProperty('hackernews')) {
      this.hackernewsConfigService.setConfig(config['hackernews'])
    }

    if (config.hasOwnProperty('refreshinterval')) {
      let t = parseInt(config['refreshinterval'])
      if (t !== NaN) {
        this.refreshInterval = t
        this.changesSubject.next({key: 'refreshinterval', config: this.refreshInterval})
      }
    }
  }

  getConfig(key): any {
    if (key === 'reddit') {
      return this.redditConfigService.getConfig()
    } else if (key === 'hackernews') {
      return this.hackernewsConfigService.getConfig()
    } else if (key === 'refreshinterval') {
      return this.refreshInterval
    } 
  }

  getObserver(): Observable<{}> {
    return this.changesSubject
  }

  isEmpty(): boolean {
    return (this.getConfig('reddit')['subnames'].concat(this.getConfig('hackernews')['pagenames']).length === 0)
  }
}
