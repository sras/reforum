import { Injectable } from '@angular/core'
import { Story } from './story'

@Injectable()
export class UtilsService {

  private convertToAssoc(stories: Story[]): {} {
    let ret = {}
    for (let s of stories) {
      ret[s.id] = s
    }
    return ret
  }

  public mergeStories(s1: Story[], s2: Story[]) {
    let s1_assoc = this.convertToAssoc(s1)
    for (let s of s2) {
      if (s1_assoc.hasOwnProperty(s.id)) {
        if (s1_assoc[s.id]['replyCount'] !== s.replyCount) {
          s1_assoc[s.id]['replyCount'] = s.replyCount
        }
        continue
      } else {
        s1.unshift(s)
      }
    }
  }

  public groupStories(stories: Story[]): Story[][] {
    let a = {}
    for (let s of stories) {
      if (a.hasOwnProperty(s.link)) {
        a[s.link].push(s)
      } else {
        a[s.link] = [s]
      }
    }

    let r: Story[][] = []
    for (let k in a) {
      if (a.hasOwnProperty(k)) {
        r.push(a[k])
      }
    }
    return r
  }

  public groupStoriesBySource(sources: Story[][]): Story[][][] {
    let d = {}
    for (let stories of sources) {
      let source_keys = stories.map(story => story.forum + story.section)
      for (let source of source_keys) {
        if (d.hasOwnProperty(source)) {
          d[source].push(stories)
        } else {
          d[source] = [stories]
        }
      }
    }

    let r: Story[][][] = []
    for (let k in d) {
      if (d.hasOwnProperty(k)) {
        r.push(d[k])
      }
    }
    return r
  }
}
