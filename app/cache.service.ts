import { Injectable } from '@angular/core'

@Injectable()
export class CacheService {
  cache: {}

  constructor() { this.cache = {} }

  setKey(key: string, value: any) {
    this.cache[key] = value
  }

  getKey(key: string) {
    if (key in this.cache) {
      return this.cache[key]
    } else {
      return null
    }
  }

  unsetKey(key: string) {
    delete this.cache[key]
  }
}
