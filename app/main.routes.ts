import { Routes } from '@angular/router';
import { StoriesComponent } from './stories.component';
import { MergedStoryCommentsComponent } from './merged.story.comments.component';
import { StoryComponent } from './story.component';
import { ConfigComponent } from './config.component';
import { SetConfigComponent } from './set.config.component';
import { WatcherComponent } from './watcher.component';

export const MainRoutes: Routes = [
  {
    path: "",
    component: StoriesComponent
  },
  {
    path: "story/:id",
    component: StoryComponent
  },
  {
    path: "config",
    component: ConfigComponent
  },
  {
    path: "setconfig",
    component: SetConfigComponent
  },
  {
    path: "merged-story",
    component: MergedStoryCommentsComponent
  },
  {
    path: "watcher",
    component: WatcherComponent
  }
];

