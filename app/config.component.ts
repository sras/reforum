import { Component, OnInit } from '@angular/core';
import { ConfigService } from './config.service';
import { RedditConfigComponent } from './reddit.config.component';
import { HackernewsConfigComponent } from './hackernews.config.component';

@Component({
  template: `
    <reddit-config></reddit-config>
    <hackernews-config></hackernews-config>
    <h3 style="margin:0px">Misc</h3>
    <div>
      <span>Refresh interval(Minutes)</span>
      <input
        class="input"
        placeholder="Refresh interval"
        type="text" [(ngModel)]="refreshInterval">
    </div>
    <a class="button" (click)="save()">Save</a>
  `,
  styles: [`
    .input {
      margin-top: 5px;
      background-color: #f7f7f7;
      border-radius: 5px;
      border: none;
      color: black;
      padding: 5px;
    }
    .button {
      margin: 10px;
      margin-left: 0px;
      display: block;
      max-width: 100px;
      padding: 5px;
      font-size: 70%;
      margin-right: 5px;
      border-radius: 5px;
      color: #ffffff;
      background-color: #424242;
      border-color: #424242;
      cursor: pointer;
    }
  `]
})
export class ConfigComponent implements OnInit { 
  refreshInterval: string

  constructor(private configService: ConfigService) {}

  ngOnInit() {
    this.refreshInterval = this.configService.getConfig('refreshinterval')
  }

  save() {
    this.configService.setConfig({refreshinterval: this.refreshInterval})
  }
}
