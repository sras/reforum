import { Injectable } from '@angular/core'
import {Observable} from 'rxjs/Observable'
import {Subject} from 'rxjs/Subject'
import {Subscription} from 'rxjs/Subscription'
import 'rxjs/add/observable/timer'
import { Story, FullStory, Comment, WatchItem, Change } from './story'
import { StoryService } from './story.service'

@Injectable()
export class WatcherService {

  private storiesSubject: Subject<Story[]> = null
  private changesSubject: Subject<Change> = null
  private watchItems: WatchItem[] = []
  private subscription: Subscription = null

  constructor(
    private storyService: StoryService
    ) {
    this.storiesSubject = new Subject() as Subject<Story[]>
    this.changesSubject = new Subject() as Subject<Change>
  }

  startWatch(interval: number) {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
    this.subscription = Observable.timer(interval * 60 * 1000, interval * 60 * 1000).subscribe((_) => {
      this.storyService.softReset()
      this.storyService.getStories().subscribe((s) => {
        this.storiesSubject.next(s)
      })
      this.processWatchedItems()
    })
  }

  getStoriesObserver(): Observable<Story[]> {
    return this.storiesSubject
  }

  getChangesObserver(): Observable<Change> {
    return this.changesSubject
  }

  getWatchedItems(): WatchItem[] {
    return this.watchItems
  }

  watchItem(item: FullStory|Comment) {
  }

  processWatchedItems() {
  }

  getChildIds(item: FullStory|Comment): {} {
    let ids: {} = {}
    if (item instanceof FullStory) {
      ids[item.story.id] = true
    } else if (item instanceof Comment) {
      ids[item.id] = true
    }
    if (item.comments) {
      for (let c of item.comments) {
        let child_ids = this.getChildIds(c)
        for (let cid in child_ids) {
          if (child_ids.hasOwnProperty(cid)) {
            ids[cid] = true
          }
        }
      }
    }
    return ids
  }

  findDiff(sig1:{}, sig2:{}): {} {
    let ret = {}
    for (let id in sig1) {
      if (sig1.hasOwnProperty(id)) {
        if (sig2.hasOwnProperty(id)) {
          continue
        } else {
          ret[id] = true
        }
      }
    }
    return ret
  }

  getChildren(item: FullStory|Comment, diff:string[]): Comment[] {
    return this._getChildren(item, diff)
  }

  _getChildren(item: FullStory|Comment, diff:string[]): Comment[]{
    let result = []
    if (item instanceof Comment) {
      if (diff.indexOf(item.id) !== -1) {
        result.push(item)
      }
    }

    if (item.comments) {
      for (let c of item.comments) {
        result = result.concat(this._getChildren(c, diff))
      }
    }
    return result
  }

  unwatch(wi: WatchItem) {
    let i = this.watchItems.indexOf(wi)
    if (i !== -1) {
      this.watchItems.splice(i, 1)
    }
  }

  checkForChange(it: WatchItem) {
  }

  notify(change: Change) {
    this.changesSubject.next(change)
  }

}
