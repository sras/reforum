import {Pipe, PipeTransform} from '@angular/core'

@Pipe({name: 'timeFormat'})
export class TimeformatPipe implements PipeTransform {
  transform(text: string): string {
    let i = parseInt(text)
    if (i === NaN) {
      return ""
    } else {
      let n = new Date()
      let d = new Date(i * 1000)
      let diff = Math.abs(n.getTime() - d.getTime())
      let  minutes = Math.floor((diff/1000)/60)
      if (minutes < 60) {
        return minutes + ' minutes before'
      } else {
        let hours = Math.floor(minutes/60)
        if (hours < 24) {
          return  hours + ' hours before'
        } else {
          let days = Math.floor(hours/24)
          return  days + ' days before'
        }
      }
    }
  }
}
