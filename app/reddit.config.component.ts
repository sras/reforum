import { Component } from '@angular/core'
import { RedditConfigService } from './reddit.config.service'
import { StoryService } from './story.service'

@Component({
  selector: 'reddit-config',
  template: `
    <div style="margin-bottom: 20px">
      <h2 style="margin:0px">Reddit config:</h2>
      <div>
        <div *ngFor="let subname of redditConfigService.subNames">
          <div>
            {{subname}} <a class="link" (click)="removeSub(subname)">Remove</a>
          </div>
        </div>
        <input
          class="input"
          placeholder="Add new sub"
          type="text" [(ngModel)]="newSub">
        <a class="button" (click)="addNewSub(newSub)">Add new</a>
      </div>
    </div>
  `,
  styles: [`
    .link {
      text-decoration: underline;
    }
    .input {
      margin-top: 5px;
      background-color: #f7f7f7;
      border-radius: 5px;
      padding: 5px;
      border: none;
      color: black;
    }
    .button {
      cursor: pointer;
      margin: 10px;
      margin-left: 0px;
      display: block;
      max-width: 100px;
      padding: 5px;
      font-size: 70%;
      margin-right: 5px;
      border-radius: 5px;
      color: #ffffff;
      background-color: #424242;
      border-color: #424242;
    }
  `]
})

export class RedditConfigComponent {
  public newSub: string = null

  constructor(
      private redditConfigService: RedditConfigService,
      private storyService: StoryService
      ) {}

  removeSub(subname) {
    this.redditConfigService.removeSub(subname)
    this.storyService.reset()
  }

  addNewSub(subname) {
    this.redditConfigService.addNewSub(subname)
    this.storyService.reset()
    this.newSub = ""
  }

}
