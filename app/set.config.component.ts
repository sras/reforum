import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from './config.service';
import { StoryService } from './story.service';
import { StoriesComponent } from './stories.component';
import { CacheService } from './cache.service'

@Component({
  template: '<stories-component [triggerReload]="triggerReload"></stories-component>'
})

export class SetConfigComponent implements OnInit {
  triggerReload: boolean = false

  constructor(
    private configService: ConfigService,
    private route: ActivatedRoute,
    private router: Router,
    private cacheService: CacheService,
    private storyService: StoryService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      let config_str = JSON.stringify(params)
      let last_config = this.cacheService.getKey('last-url-config')
      if (last_config && last_config===config_str) {
        return
      } else {
        this.cacheService.setKey('last-url-config', config_str)
        this.storyService.reset()
        this.triggerReload = !this.triggerReload
        this.configService.setConfig(params)
      }
    })
  }
}
