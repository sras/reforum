import { Component, OnInit } from '@angular/core'
import { StoriesComponent } from './stories.component';
import { StoryComponent } from './story.component';
import { ConfigComponent } from './config.component';
import { SetConfigComponent } from './set.config.component';
import { LogComponent } from './log.component';
import { ConfigWatcherService } from './config.watcher.service';
import { ConfigService } from './config.service';
import { WatcherService } from './watcher.service';
import { WatcherComponent } from './watcher.component';
import { StoryItemComponent } from './story.item.component';
import { NotificationService } from './notification.service';

@Component({
  template: `
  <div class="pure-g">
    <div class="pure-u-1">
        <log></log>
        <div *ngIf="empty">
          No story sources found!
        </div>
      <router-outlet></router-outlet>
    </div>
  <div>
  `,
  styles: [`
    .top-button {
      display: block;
      padding: 5px;
      font-size: 70%;
      float: right;
      margin-right: 5px;
      border-radius: 5px;
      color: #ffffff;
      background-color: #424242;
      border-color: #424242;
    }
  .log-message {
    font-size: 12px;
    padding: 10px;
  }

  `]
  //directives: [StoryItemComponent, WatcherComponent, LogComponent, ROUTER_DIRECTIVES, StoriesComponent, ConfigComponent],
  //precompile: [StoryItemComponent, WatcherComponent, LogComponent, StoriesComponent, StoryComponent, ConfigComponent, SetConfigComponent]
})
export class MainComponent implements OnInit {
  empty: boolean = false
  constructor(
    private configWatcherService: ConfigWatcherService,
    private configService: ConfigService,
    private watcherService: WatcherService,
    private notificationService: NotificationService
    ) {
  }

  ngOnInit() {
    this.empty = this.configService.isEmpty()
    this.configService.getObserver().subscribe((config) => {
      if (config['key'] === 'refreshinterval') {
        this.watcherService.startWatch(config['config'])
      }
      this.empty = this.configService.isEmpty()
    })
    this.configWatcherService.start()
  }



}
