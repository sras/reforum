import {Pipe, PipeTransform} from '@angular/core'

@Pipe({name: 'entityDecode'})
export class EntityDecodePipe implements PipeTransform {
  transform(text: string): string {
    var html = document.createElement("textarea");
    html.innerHTML = text;
    return html.value;
  }
}
